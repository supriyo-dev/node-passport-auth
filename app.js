const express=require('express')
const mongoose=require('mongoose')
const expressLayouts=require('express-ejs-layouts')
const app=express()
const flash= require('connect-flash')
const session=require('express-session')
const PORT = process.env.PORT=6050
const passport=require('passport')


require('./config/passport')(passport)
//passport initialize
app.use(passport.initialize());
app.use(passport.session());
//DB
const db= (require('./config/keys')).MongoURI

//Views
app.use(expressLayouts)
app.set('view engine','ejs')

//Body Parser
app.use(express.urlencoded({ extended:false}))




//Express Session
app.use(session({
        secret: 'session',
        resave: false,
        saveUninitialized: true  
      }))

//Connect Flash
app.use(flash())

//Set Global Msg
app.use((req,res,next)=>{
     res.locals.success_msg=req.flash('success_msg')
     res.locals.error_msg=req.flash('error_msg')
     res.locals.error=req.flash('error')
     next();     
})      



//Routes
require(('./routes/index'))(app,passport)
require('./routes/users')(app,passport)
// app.use('/',require('./routes/index'))(app)
// app.use('/users',require('./routes/users'))

mongoose.connect(db , {useNewUrlParser: true,useUnifiedTopology: true})
        .then(()=>console.log('Mongo Connected...'))
        .catch(err=>console.log(err));

app.listen(PORT,console.log(`Server running on ${PORT}`))

