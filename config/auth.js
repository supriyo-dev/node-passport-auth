module.exports={
    ensureAuthenticated: function (req,res,next){
        console.log('autheee',req.isAuthenticated())
        if(req.isAuthenticated()){
            next();
        }
        req.flash('error_msg','Please Login to view the resource');
        res.redirect('/users/login')
    },
  forwardAuthenticated: function(req, res, next) {
    if (!req.isAuthenticated()) {
      return next();
    }
    res.redirect('/dashboard');      
  }
}