// const express= require('express')
// const router = express.Router();
const bcrypt= require('bcryptjs')
const User =require('../models/User')


module.exports= function(app,passport){
app.get('/users/login',(req,res)=>res.render('login'))
app.post ('/users/login',(req,res,next)=>{
    passport.authenticate('local',{
        successRedirect:'/dashboard',
        failureRedirect:'/users/login',
        failureFlash:true
    })(req,res,next);
})
app.get('/users/register',(req,res)=>res.render('register'))
app.get('/users/logout',(req,res)=>{
    req.logout();
    req.flash('success','Successfully Logout')
    res.redirect('/users/login')
})

app.post('/users/register',(req,res)=>{
    console.log(req.body)
    const {name,email,password,password2}=req.body
    var errors=[]
    if(!name || !email || !password || !password2){
        errors.push({msg:"All fields are required."})
    }
    if(password !== password2){
        errors.push({msg:"Password do not match"})
    }
    if(password < 6){
        errors.push({msg:"Password length should be atleast 6 characters."})
    }

    if(errors.length > 0) {
        res.render('register',{
            errors,
            name,
            email,
            password,
            password2
        })
    } else {
        User.findOne({  email:email })
            .then(user=>{
                console.log(user)
                if(user){
                    errors.push({msg:"Email Already Registered."})
                    res.render('register',{
                        errors,
                        name,
                        email,
                        password,
                        password2
                    })
                } else {
                    const newUser=new User({
                        name,
                        email,
                        password
                    })
                    bcrypt.genSalt(10, (err,salt)=>{
                        bcrypt.hash(newUser.password,salt,(err,hash)=>{
                            if(err) throw err
                            newUser.password=hash
                            newUser.save()
                                    .then(user =>{
                                        req.flash('success_msg','You are successfully registered.Now you can log in')
                                        res.redirect('/users/login')
                                    })
                                    .catch(err=>console.log(err))
                        })
                    })

                }
            })
       
    //    res.send('Success')
    }
})


}





// module.exports=router;